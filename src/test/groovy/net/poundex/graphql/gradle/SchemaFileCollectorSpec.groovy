package net.poundex.graphql.gradle

import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path

class SchemaFileCollectorSpec extends Specification implements TestData {
	void "Gather all schemas from dependencies and write to build directory"() {
		given: "A collection of dependencies which provide GraphQL schemas"
		Path depsDir = Files.createTempDirectory("gqlgrdl")
		List<Path> depFiles = filesFromDependencies(depsDir, "depsSimple", "one", "two", "three")
		
		and: "A collector instance for those dependencies"
		Path outputDirectory = Files.createTempDirectory("gqlgrdl")
		SchemaFileCollector collector = new SchemaFileCollector(outputDirectory, depFiles*.toFile())
		
		when: "The schema files are collected"
		collector.collectSchemaFiles()
		List<Path> written = Files.list(outputDirectory).toList()
		
		then: "Schema files from all dependencies are written"
		written.size() == 4
		written.collect { it.readLines().findAll() }.flatten().toSet() == ["One", "Two", "Three", "Three 2"].toSet()
	}
}
