package net.poundex.graphql.gradle

import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path

class SuperSchemaRendererSpec extends Specification implements TestData {
	
	void "Find and merge all dependency schemas into a single schema"() {
		given: "A collection of dependencies which provide GraphQL schemas"
		Path depsDir = Files.createTempDirectory("gqlgrdl")
		List<Path> depFiles = filesFromDependencies(depsDir, "depsFed", "product", "review")

		and: "A SuperSchemaRenderer instance for those dependencies"
		SuperSchemaRenderer renderer = new SuperSchemaRenderer(depFiles*.toFile())
		
		when:
		String combined = renderer.getFullSchema()
		
		then:
		combined.contains 'type Product @key(fields: "id") {'
		combined.contains 'type Product @key(fields: "id") @extends {'
		combined.contains 'type Review {'
	}
	
	void "Replace all occurrences of extends directive with keyword"() {
		given: "An input schema with multiple uses of extends directive"
		String inputQuery = """\
type One @extends {
	one: ID!
}

type Two @unrelated @extends {
	two: ID!
}

type Three @extends @unrelated {
	three: ID!
}
"""
		
		and: "A SuperSchemaRenderer instance"
		SuperSchemaRenderer renderer = new SuperSchemaRenderer(null)
		
		when: "The input query is passed to the replacer"
		String output = renderer.replaceExtendsDirectivesWithKeywords(inputQuery)
		
		then: "All instances of the extends directive are replaced with the extend keyword (keeping other directives)"
		output.contains 'extend type One {'
		output.contains 'extend type Two @unrelated {'
		output.contains 'extend type Three @unrelated {'
		
		and: "All other lines remain unchanged"
		(inputQuery.readLines().toSet() - output.readLines().toSet()).size() == 3
	}
	
	void "Insert base type definition for Query when all sub-schemas provide only partials"() {
		given: "A composite input query where all Query definitions are partials"
		String inputQuery = """\
extend type Query {
	foo: String
}

extend type Query {
	bar: String
}
"""
		and: "A SuperSchemaRenderer instance"
		SuperSchemaRenderer renderer = new SuperSchemaRenderer([])
		
		when: "Rendering input schemas that provide only partial definitions for the Query type"
		String outputSchema = renderer.doRenderSuperSchema(inputQuery)
		
		then: "The base type is inserted and the schema builds"
		outputSchema.readLines().findAll { it == "type Query {" }.size() == 1
		with(outputSchema.substring(outputSchema.indexOf("type Query {"))) {
			it.contains("foo: String")
			it.contains("bar: String")
		}
	}
	
	void "Produce a single schema containing all schemas from all dependencies"() {
		given: "A collection of dependencies which provide GraphQL schemas"
		Path depsDir = Files.createTempDirectory("gqlgrdl")
		List<Path> depFiles = filesFromDependencies(depsDir, "depsFed", "product", "review")
		
		and: "A SuperSchemaRenderer for those dependency schemas"
		SuperSchemaRenderer renderer = new SuperSchemaRenderer(depFiles*.toFile())
		
		when: "The super schema is rendered"
		String outputSchema = renderer.renderSuperSchema()
		
		then: "The output schema is a combination of all input schemas"
		outputSchema.contains("type Product")
		outputSchema.contains("type Review")
		outputSchema.contains("type Query")
		
		and: "The output schema contains no federation metadata"
		! outputSchema.contains("@key")
		! outputSchema.contains("@external")
		! outputSchema.contains("@requires")
		! outputSchema.contains("@provides")
	}
}
