package net.poundex.graphql.gradle


import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Path
import java.util.stream.Collectors
import java.util.stream.Stream

class SchemaFileMergerSpec extends Specification implements TestData {

	void "Create output directory tree"() {
		given: "A working directory"
		Path workDir = Files.createTempDirectory("gqlgrdl")

		and: "An instance pointing at a target under the working directory"
		Path outputFile = workDir.resolve("put/the/files/here/output.graphqls")
		SchemaFileMerger fileMerger = new SchemaFileMerger(outputFile, null, null)

		when: "ensureOutputDirectory() is called"
		fileMerger.ensureOutputDirectory()

		then: "The output file and any parent directories are created"
		Files.exists(outputFile)
	}

	void "Remove an existing output file if it already exists"() {
		given: "A working directory"
		Path workDir = Files.createTempDirectory("gqlgrdl")

		and: "An instance pointing at a target under the working directory"
		Path outputFile = workDir.resolve("put/the/files/here/output.graphqls")
		SchemaFileMerger fileMerger = new SchemaFileMerger(outputFile, null, null)

		and: "The file already exists with content"
		fileMerger.ensureOutputDirectory()
		Files.writeString(outputFile, "Content in the file")

		when: "ensureOutputDirectory() is called"
		fileMerger.ensureOutputDirectory()

		then: "The output file and any parent directories are created"
		Files.exists(outputFile)

		and: "The file is replaced with an empty one"
		Files.readString(outputFile).empty
	}

	void "Return InputStreams for all GraphQL files found in dependencies"() {
		given: "A collection of dependencies which provide GraphQL schemas"
		Path depsDir = Files.createTempDirectory("gqlgrdl")
		List<Path> depFiles = filesFromDependencies(depsDir, "depsSimple", "one", "two", "three")

		and: "A merger instance for those dependencies"
		SchemaFileMerger fileMerger = new SchemaFileMerger(null, depFiles*.toFile(), null)

		when: "GraphQL schemas are located in project dependencies"
		Stream<InputStream> ret = fileMerger.getDependencySchemas()
		List<InputStream> retList = ret.collect(Collectors.toList())

		then: "All schemas are found"
		retList.size() == 4

		and: "An InputStream to each schema is provided"
		retList[0].text.startsWith("One")
		retList[1].text.startsWith("Two")
		retList[2].text.startsWith("Three")
		retList[3].text.startsWith("Three 2")
	}

	void "Return InputStreams for all GraphQL files found in this project"() {
		given: "A collection of GraphQL schemas"
		Path schemaSourcesDir = Files.createTempDirectory("gqlgrdl")
		filesFromThisProject(schemaSourcesDir, "one", "two", "three")

		and: "A merger instance for those schemas"
		SchemaFileMerger fileMerger = new SchemaFileMerger(null, null, schemaSourcesDir)

		when: "GraphQL schemas are located in this project"
		Stream<InputStream> ret = fileMerger.getThisProjectSchemas()
		List<InputStream> retList = ret.collect(Collectors.toList())

		then: "All schemas are found"
		retList.size() == 3

		and: "An InputStream to each schema is provided"
		with(retList*.text*.trim()) {
			it.contains("One")
			it.contains("Two")
			it.contains("Three")
		}
	}
	
	void "Write the result of merging all schemas to the target output file"() {
		given: "A collection of GraphQL schemas from both dependencies and this project"
		Path schemaSourcesDir = Files.createTempDirectory("gqlgrdl")
		filesFromThisProject(schemaSourcesDir, "one", "two")
		
		Path depsDir = Files.createTempDirectory("gqlgrdl")
		List<Path> depFiles = filesFromDependencies(depsDir, "depsSimple", "three")

		and: "A merger instance for those schemas"
		Path targetFile = Files.createTempFile(Files.createTempDirectory("gqlgrdl"), "merged-schemas", ".graphqls")
		SchemaFileMerger fileMerger = new SchemaFileMerger(targetFile, depFiles*.toFile(), schemaSourcesDir)
		
		when: "Schemas are merged from all sources"
		fileMerger.writeMergedSchemas()
		
		then: "The target file contains the merged result of all the schemas"
		targetFile.readLines().containsAll(["One", "Two", "Three", "Three 2"])
	}
	
}
