package net.poundex.graphql.gradle

import graphql.language.AstTransformer
import graphql.language.Document
import graphql.language.NodeVisitor
import graphql.parser.Parser
import graphql.schema.idl.SchemaPrinter
import spock.lang.Specification
import spock.lang.Subject

class FederationMetadataRemovingNodeVisitorSpec extends Specification {
	
	@Subject
	FederationMetadataRemovingNodeVisitor nodeVisitor = new FederationMetadataRemovingNodeVisitor()
	
	void "Remove all federation directives from schema"() {
		given: "An input schema with federation metadata"
		String inputSchema = """\
type Review @key(fields: "id") {
  product: Product @provides(fields: "name")
}

type Product @key(fields: "upc") {
  upc: String @external
  name: String @external
  reviews: [Review] @requires(fields: "email")
}

type Query { 
	noop: String
} 
"""
		when: "The schema is transformed"
		String transformed = transformSchemaWithVisitor(inputSchema, nodeVisitor)
		
		then: "The schema has all the federation specific directives removed"
		! transformed.contains("@key")
		! transformed.contains("@requires")
		! transformed.contains("@provides")
		! transformed.contains("@external")
	}
	
	void "Remove 'duplicate' fields supplied by partials"() {
		given: "An input schema with federation metadata"
		String inputSchema = """\
type Foo {
	id: ID!
	first: String
}

extend type Foo {
	id: ID!
	second: String
}

type Query { 
	noop: String
} 
"""
		when: "The schema is transformed"
		String transformed = transformSchemaWithVisitor(inputSchema, nodeVisitor)

		then: "The schema flattens re-declared fields"
		transformed.readLines().findAll { it == "type Foo {" }.size() == 1
		with(transformed.substring(transformed.indexOf("type Foo {"))) {
			it.contains("id: ID!")
			it.contains("first: String")
			it.contains("second: String")
		}
	}
	
	private static String transformSchemaWithVisitor(String inputSchema, NodeVisitor nodeVisitor) {
		Document document = new Parser().parseDocument(inputSchema)
		AstTransformer astTransformer = new AstTransformer()
		Document xformed = astTransformer.transform(document, nodeVisitor) as Document
		return new SchemaPrinter().print(xformed)
	}
}
