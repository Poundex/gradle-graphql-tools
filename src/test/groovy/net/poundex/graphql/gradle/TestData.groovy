package net.poundex.graphql.gradle

import com.google.common.io.Resources

import java.nio.file.Files
import java.nio.file.Path

trait TestData {
	
	List<Path> filesFromDependencies(Path depsDir, String type, String... presets) {
		return presets.collect { depName ->
			Path depFile = depsDir.resolve("${depName}.zip")
			Resources.copy(Resources.getResource("${type}/${depName}.zip"),
					Files.newOutputStream(depFile))
			return depFile
		}
	}

	void filesFromThisProject(Path schemaSourcesDir, String... presets) {
		presets.each { schemaName ->
			Path depFile = schemaSourcesDir.resolve("${schemaName}.graphqls")
			Resources.copy(Resources.getResource("someSchemas/${schemaName}.graphqls"),
					Files.newOutputStream(depFile))
			return depFile
		}
	}
}