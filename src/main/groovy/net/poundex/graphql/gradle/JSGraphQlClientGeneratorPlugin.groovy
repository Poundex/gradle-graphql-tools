package net.poundex.graphql.gradle

import com.github.gradle.node.npm.task.NpxTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class JSGraphQlClientGeneratorPlugin implements Plugin<Project> {
	@Override
	void apply(Project project) {
		
		project.plugins.apply(NodeModulePlugin)
		
		PrepareNodeModuleTask prepareNodeModuleTask = project.tasks.getByName("prepareNodeModule") as PrepareNodeModuleTask
		prepareNodeModuleTask.configure {
			moduleName = project.name
			moduleDeps << "graphql"
			moduleDeps << "@graphql-codegen/cli"
			moduleDeps << "@graphql-codegen/typescript"
		}

		GenerateJSClientGenConfigTask genConfigTask = project.tasks.create("writeJSClientGenConfig", GenerateJSClientGenConfigTask)
		genConfigTask.dependsOn(prepareNodeModuleTask)
		
		NpxTask genClient = project.tasks.create("generateJsClient", NpxTask)
		genClient.configure {
			command = 'graphql-codegen'
			args = ['generate']
		}
		
		genClient.dependsOn(genConfigTask)
		genClient.dependsOn(prepareNodeModuleTask)
		
		project.tasks.getByName("assemble").dependsOn(genClient)
		genConfigTask.dependsOn(project.configurations.schema)
		genConfigTask.dependsOn(project.tasks.getByName("collectSchemaFiles"))
	}
}
