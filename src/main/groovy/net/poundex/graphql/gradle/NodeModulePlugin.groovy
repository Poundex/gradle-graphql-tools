package net.poundex.graphql.gradle

import com.github.gradle.node.NodeExtension
import com.github.gradle.node.NodePlugin
import org.gradle.api.Plugin
import org.gradle.api.Project

class NodeModulePlugin implements Plugin<Project> {
	@Override
	void apply(Project project) {
		project.plugins.apply(NodePlugin)
		NodeExtension nodeExtension = project.extensions.getByType(NodeExtension)
		nodeExtension.download.set(true)
		
		PrepareNodeModuleTask prepareNodeModuleTask = project.tasks.create("prepareNodeModule", PrepareNodeModuleTask)
		
		prepareNodeModuleTask.dependsOn(project.tasks.getByName("npmSetup"))
		
		project.afterEvaluate {
			nodeExtension.getNodeProjectDir().set(new File(project.buildDir, "npm-${prepareNodeModuleTask.moduleName}"))
		}
	}
}
