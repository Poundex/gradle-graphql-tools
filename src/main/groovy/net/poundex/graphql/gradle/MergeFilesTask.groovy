package net.poundex.graphql.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import java.nio.file.Path

class MergeFilesTask extends DefaultTask {
	@TaskAction
	void mergeFiles() {
		new SchemaFileMerger(outputFile, projectDependencies, srcDir).writeMergedSchemas()
	}

	@OutputFile
	Path getOutputFile() {
		String outputFilename = 
				project.extensions.getByType(GraphQlSchemaPlugin.Extension).getOutputFilename(project)
		return project.buildDir.toPath().resolve("tmp/${outputFilename}.graphqls")
	}

	@InputDirectory
	Path getSrcDir() {
		return project.projectDir.toPath()
				.resolve("src/main/resources/graphql")
	}

	@InputFiles
	Iterable<File> getProjectDependencies() {
		return project.configurations.compileClasspath
	}
}
