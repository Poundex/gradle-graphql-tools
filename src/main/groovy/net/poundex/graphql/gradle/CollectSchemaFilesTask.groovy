package net.poundex.graphql.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

import java.nio.file.Path

class CollectSchemaFilesTask extends DefaultTask {

	@TaskAction
	void collectSchemaFiles() {
		new SchemaFileCollector(outputDirectory, project.configurations.schema).collectSchemaFiles()
	}
	
	@OutputDirectory
	Path getOutputDirectory() {
		return project.buildDir.toPath().resolve("schemas")
	}
}
