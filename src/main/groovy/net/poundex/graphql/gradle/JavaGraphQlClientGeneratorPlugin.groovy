package net.poundex.graphql.gradle

import com.netflix.graphql.dgs.codegen.gradle.CodegenPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

class JavaGraphQlClientGeneratorPlugin implements Plugin<Project> {

	@Override
	void apply(Project project) {
		GraphQlSchemaDependenciesPlugin.applyIfNeeded(project)
		
		if( ! project.plugins.hasPlugin(CodegenPlugin))
			project.plugins.apply(CodegenPlugin)
		
		Task collectSchemas = project.tasks.create("collectSchemaFiles", CollectSchemaFilesTask)

		Task generateJava = project.tasks.getByName("generateJava")
		project.extensions.create("gqlJavaClient", Extension)
		
		project.afterEvaluate {
			generateJava.configure {
				schemaPaths = ["${project.buildDir}/schemas"]
				packageName = project.extensions.getByType(Extension).packageName
				generateClient = true
			}
		}
		
		collectSchemas.dependsOn(project.configurations.schema)
		generateJava.dependsOn(collectSchemas)
	}
	
	static class Extension {
		String packageName
	}
}
