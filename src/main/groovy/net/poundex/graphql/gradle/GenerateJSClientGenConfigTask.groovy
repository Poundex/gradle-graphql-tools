package net.poundex.graphql.gradle

import com.fasterxml.jackson.databind.ObjectMapper
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import java.nio.file.Path

class GenerateJSClientGenConfigTask extends DefaultTask {
	
	@Input
	Map<String, List<String>> genConfig = [:]
	
	@TaskAction
	void generateConfig() {
		Map config = [
				schema: "${schemasDir.toString()}/*".toString(),
				generates: genConfig.collectEntries {out, plugins ->
					[(out): [plugins: plugins]]
				}
		]
		configFile.text = new ObjectMapper().writeValueAsString(config)
	}
	
	@InputDirectory
	Path getSchemasDir() {
		return project.buildDir.toPath().resolve("schemas")
	}
	
	@OutputFile
	Path getConfigFile() {
		return project.buildDir.toPath().resolve("npm-" +
				((PrepareNodeModuleTask) project.tasks.getByName("prepareNodeModule")).moduleName)
				.resolve("codegen.json")
	}
}
