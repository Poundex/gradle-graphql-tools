package net.poundex.graphql.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import java.nio.file.Path

class RenderSuperSchemaTask extends DefaultTask {

	@TaskAction
	void renderSuperSchema() {
		Configuration schema = project.configurations.getByName("schema")
		outputFile.text = new SuperSchemaRenderer(schema).renderSuperSchema()
	}
	
	@OutputFile
	Path getOutputFile() {
		String outputFilename = project.extensions.getByType(GraphQlSuperSchemaPlugin.Extension).outputFilename
		if( ! outputFilename)
			outputFilename = project.name
		return project.buildDir.toPath().resolve("tmp/${outputFilename}.graphqls")
	}
}
