package net.poundex.graphql.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration

class GraphQlSchemaDependenciesPlugin implements Plugin<Project> {
	
	static applyIfNeeded(Project project) {
		if( ! project.plugins.hasPlugin(GraphQlSchemaDependenciesPlugin))
			project.plugins.apply(GraphQlSchemaDependenciesPlugin)
	}
	
	@Override
	void apply(Project project) {
		Configuration schemaConfiguration = project.configurations.create("schema")
		schemaConfiguration.transitive = false
	}
}
