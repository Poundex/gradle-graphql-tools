package net.poundex.graphql.gradle

import com.github.gradle.node.npm.task.NpmTask
import groovy.text.SimpleTemplateEngine
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction

import java.nio.file.Files
import java.nio.file.Path

class PrepareNodeModuleTask extends DefaultTask {
	@Input
	String moduleName = "unset"
	
	@Input
	List<String> moduleDeps = []
	
	@TaskAction
	void prepareNodeModule() {
		Files.createDirectories(outDir)
		outDir.resolve("package.json").text = new SimpleTemplateEngine()
				.createTemplate(PACKAGE_JSON_TEMPLATE)
				.make([packageName: moduleName])
		
		moduleDeps.each { depName ->
			NpmTask t = project.tasks.create("__installDepInternal_${depName}"
					.replaceAll("/", "_")
					.replaceAll("\\\\", "_"), NpmTask)
			
			t.configure {
				args = ['install', depName, '--save-dev']
			}
			t.exec()
		}
	}
	
	@OutputDirectory
	Path getOutDir() {
		project.buildDir.toPath().resolve("npm-${moduleName}")
	}
	
	private static final PACKAGE_JSON_TEMPLATE = '''\
{
  "name": "${packageName}",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \\"Error: no test specified\\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
'''
}
