package net.poundex.graphql.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.language.jvm.tasks.ProcessResources

class GraphQlSuperSchemaPlugin implements Plugin<Project> {
	
	@Override
	void apply(Project project) {
		GraphQlSchemaDependenciesPlugin.applyIfNeeded(project)
		project.extensions.create("gqlSuperSchema", Extension)
		
		RenderSuperSchemaTask renderSuperSchemaTask = project.tasks.create("renderSuperSchema", RenderSuperSchemaTask)

		ProcessResources processResources = project.tasks.getByName("processResources") as ProcessResources
		processResources.dependsOn(renderSuperSchemaTask)

		processResources.into("graphql") {
			from(renderSuperSchemaTask.outputFile.toString())
		}
	}
	
	static class Extension {
		String outputFilename = ""
	}
}