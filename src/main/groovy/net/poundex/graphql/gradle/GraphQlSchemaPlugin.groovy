package net.poundex.graphql.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.language.jvm.tasks.ProcessResources

import java.util.function.Function

class GraphQlSchemaPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.extensions.create("gqlSchema", Extension)
        
        MergeFilesTask mergeFilesTask = project.tasks.create("mergeSchemaFiles", MergeFilesTask)

        ProcessResources processResources = project.tasks.getByName("processResources") as ProcessResources
        processResources.dependsOn(mergeFilesTask)

        processResources.exclude("graphql/")

        processResources.into("graphql") {
            from(mergeFilesTask.outputFile.toString())
        }
    }
    
    static class Extension {
        {
            setOutputFilename { Project p -> p.name }
        }
        
        Object outputFilename
        
        void setOutputFilename(String filename) {
            this.@outputFilename = filename 
        }
        
        void setOutputFilename(Function<Project, String> fn) {
            this.@outputFilename = fn
        }
        
        String getOutputFilename(Project project) {
            return outputFilename instanceof Function 
                    ? outputFilename.apply(project) 
                    : outputFilename.toString()
        }
    }
}
