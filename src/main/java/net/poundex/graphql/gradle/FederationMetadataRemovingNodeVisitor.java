package net.poundex.graphql.gradle;

import graphql.language.*;
import graphql.util.TraversalControl;
import graphql.util.TraverserContext;
import graphql.util.TreeTransformerUtil;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class FederationMetadataRemovingNodeVisitor extends NodeVisitorStub {
    
    private static final Set<String> FEDERATION_DIRECTIVES = Set.of("key", "external", "requires", "provides");
    
    private final Map<String, Set<String>> seenFieldsTypeToField = new HashMap<>();

    @Override
    public TraversalControl visitDirective(Directive node, TraverserContext<Node> context) {
        if(FEDERATION_DIRECTIVES.contains(node.getName())) 
            return TreeTransformerUtil.deleteNode(context);
        else
            return super.visitDirective(node, context);
    }

    @Override
    public TraversalControl visitFieldDefinition(FieldDefinition node, TraverserContext<Node> context) {
        if( ! seenFieldsTypeToField.containsKey(((TypeDefinition<?>) context.getParentNode()).getName()))
            seenFieldsTypeToField.put(((TypeDefinition<?>) context.getParentNode()).getName(), new HashSet<>());

        if(seenFieldsTypeToField.get(((TypeDefinition<?>) context.getParentNode()).getName()).contains(node.getName()))
            return TreeTransformerUtil.deleteNode(context);

        seenFieldsTypeToField.get(((TypeDefinition<?>) context.getParentNode()).getName()).add(node.getName());
        return super.visitFieldDefinition(node, context);
    }
}
