package net.poundex.graphql.gradle.util;

import net.poundex.graphql.gradle.EntryInZipFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.util.zip.ZipFile;

public class ZipUtil {
    public static ZipFile newZipFile(File file) {
        try {
            return new ZipFile(file);
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not open Zip file at %s", file), ioex);
        }
    }
    
    public static InputStream getInputStream(EntryInZipFile entryInZipFile) {
        try {
            return entryInZipFile.file().getInputStream(entryInZipFile.entry());
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not open InputStream for entry in Zip %s", entryInZipFile), ioex);
        }
    }
    
    public static Stream<EntryInZipFile> getSchemaEntriesInZipFile(ZipFile zf) {
        return Collections.list(zf.entries()).stream()
                .map(ze -> new EntryInZipFile(zf, ze))
                .filter(zfe -> zfe.entry().getName().endsWith(".graphqls"));
    }
    
    public static Stream<EntryInZipFile> getAllSchemaEntriesInZipFiles(Iterable<File> zipFiles) {
        return StreamSupport.stream(zipFiles.spliterator(), false)
                .map(ZipUtil::newZipFile)
                .flatMap(ZipUtil::getSchemaEntriesInZipFile);
    }
}
