package net.poundex.graphql.gradle.util;

import com.google.common.io.ByteStreams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class FileUtil {
    public static void delete(Path path) {
        try {
            Files.delete(path);
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not delete file at %s", path), ioex);
        }
    }
    
    public static Path createDirectories(Path path) {
        try {
            return Files.createDirectories(path);
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not create directories at %s", path), ioex);
        }
    }
    
    public static Path createFile(Path path) {
        try {
            return Files.createFile(path);
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not create file at %s", path), ioex);
        }
    }
    
    public static Stream<Path> walk(Path path) {
        try {
            return Files.walk(path);
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not walk directory at %s", path), ioex);
        }
    }
    
    public static InputStream newInputStream(Path path) {
        try {
            return Files.newInputStream(path);
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not open InputStream for %s", path), ioex);
        }
    }
    
    public static Path write(Path path, Iterable<? extends CharSequence> lines) {
        try {
            return Files.write(path, lines);
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not write to file at %s", path), ioex);
        }
    }
    
    public static Path write(Path path, InputStream inputStream) {
        try {
            return Files.write(path, ByteStreams.toByteArray(inputStream));
        } catch (IOException ioex) {
            throw new RuntimeException(String.format("Could not write to file at %s", path), ioex);
        }
    }

    public static Stream<String> readLines(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream)).lines();
    }
}
