package net.poundex.graphql.gradle;

import com.google.common.annotations.VisibleForTesting;
import lombok.RequiredArgsConstructor;
import net.poundex.graphql.gradle.util.FileUtil;
import net.poundex.graphql.gradle.util.ZipUtil;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class SchemaFileMerger {
    
    private final Path outputFile;
    private final Iterable<File> projectDependencies;
    private final Path thisProjectSchemaSourcesDir;
    
    @VisibleForTesting
    void ensureOutputDirectory() {
        if (Files.exists(outputFile)) 
            FileUtil.delete(outputFile);

        FileUtil.createDirectories(outputFile.getParent());
        FileUtil.createFile(outputFile);
    }
    
    @VisibleForTesting
    Stream<InputStream> getDependencySchemas() {
        return ZipUtil.getAllSchemaEntriesInZipFiles(projectDependencies)
                .map(ZipUtil::getInputStream);
    }
    
    @VisibleForTesting
    Stream<InputStream> getThisProjectSchemas() {
		return FileUtil.walk(thisProjectSchemaSourcesDir)
				.filter(Files::isRegularFile)
				.filter(p -> p.getFileName().toString().endsWith(".graphqls"))
				.map(FileUtil::newInputStream);
    }
    
    public void writeMergedSchemas() {
		FileUtil.write(outputFile, Stream.concat(getDependencySchemas(), getThisProjectSchemas())
				.flatMap(FileUtil::readLines)
				.toList());
    }
}
