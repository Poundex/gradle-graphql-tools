package net.poundex.graphql.gradle;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public record EntryInZipFile(ZipFile file, ZipEntry entry) { }
