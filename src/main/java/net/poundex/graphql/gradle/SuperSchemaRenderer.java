package net.poundex.graphql.gradle;

import com.google.common.annotations.VisibleForTesting;
import graphql.language.AstTransformer;
import graphql.language.Document;
import graphql.language.ObjectTypeDefinition;
import graphql.language.ObjectTypeExtensionDefinition;
import graphql.parser.Parser;
import graphql.schema.idl.SchemaPrinter;
import lombok.RequiredArgsConstructor;
import net.poundex.graphql.gradle.util.FileUtil;
import net.poundex.graphql.gradle.util.ZipUtil;

import java.io.File;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class SuperSchemaRenderer {
    
    private final Iterable<File> dependencySchemas;
    
    @VisibleForTesting
    String getFullSchema() {
        return ZipUtil.getAllSchemaEntriesInZipFiles(dependencySchemas)
                .map(ZipUtil::getInputStream)
                .flatMap(FileUtil::readLines)
                .collect(Collectors.joining("\n"));
    }
    
    @VisibleForTesting
    String replaceExtendsDirectivesWithKeywords(String input) {
        return input.lines().map(s -> {
            if( ! s.contains("@extends"))
                return s;
            
            return "extend " + s.replace(" @extends ", " ");
        }).collect(Collectors.joining("\n"));
    }
    
    @VisibleForTesting
    String doRenderSuperSchema(String inputSchema) {
        Document document = new Parser().parseDocument(inputSchema);
        AstTransformer astTransformer = new AstTransformer();

        Optional<ObjectTypeDefinition> rootLevelQuery = document.getDefinitionsOfType(ObjectTypeDefinition.class)
                .stream()
                .filter(otd -> otd.getName().equals("Query"))
                .filter(otd -> ! (otd instanceof ObjectTypeExtensionDefinition))
                .findFirst();

        if(rootLevelQuery.isEmpty())
            return doRenderSuperSchema(String.format("type Query { }\n%s", inputSchema));
        
        Document newDocument = (Document) astTransformer.transform(document, new FederationMetadataRemovingNodeVisitor());

        return new SchemaPrinter().print(newDocument);
    }
    
    public String renderSuperSchema() {
        return doRenderSuperSchema(
                replaceExtendsDirectivesWithKeywords(
                        getFullSchema()));
    }
}
