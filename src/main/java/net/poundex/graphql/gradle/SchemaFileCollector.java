package net.poundex.graphql.gradle;

import lombok.RequiredArgsConstructor;
import net.poundex.graphql.gradle.util.FileUtil;
import net.poundex.graphql.gradle.util.ZipUtil;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
public class SchemaFileCollector {
    private final Path outputDirectory;
    private final Iterable<File> dependenciesWithSchemas;
    
    public void collectSchemaFiles() {
        ZipUtil.getAllSchemaEntriesInZipFiles(dependenciesWithSchemas)
                .forEach(schemaFileEntry -> {
                    Path outFile = Paths.get(schemaFileEntry.entry().getName()).getFileName();
                    FileUtil.write(outputDirectory.resolve(outFile), ZipUtil.getInputStream(schemaFileEntry));
                });
    }
}
